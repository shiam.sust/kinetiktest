<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admins', 'namespace' => 'Admins'], function () {
    Route::post('login', 'AdminsController@adminsLogin');
    
});

Route::group(['prefix' => 'admins', 'namespace' => 'Admins', 'middleware' => ['adminsRoutes'] ], function () {
    Route::post('add-customer', 'CustomerController@store');
    Route::get('customers', 'CustomerController@getCustomers');
    Route::post('add-bill', 'BillController@store');
    Route::get('bills/{id}', 'BillController@getBills');
    Route::post('change-bill-status/{id}', 'BillController@updateBillStatus');
    Route::get('get-customer-info/{id}', 'CustomerController@getTheCustomer');
    Route::post('edit-customer', 'CustomerController@editCustomer');
    Route::post('delete-customer/{id}', 'CustomerController@deleteCustomer');
});

