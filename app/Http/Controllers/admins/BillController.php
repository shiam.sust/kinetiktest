<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bills;

class BillController extends Controller
{
    public function store(Request $request){
        $bill = new Bills();

        $bill->customer_id = $request->customerId;
        $bill->month = $request->month;
        $bill->year = $request->year;
        $bill->amount = $request->amount;

        $bill->save();

        return response()->json(['new bill added!']);
    }

    public function getBills($id)
    {
        $bills = Bills::where('customer_id', $id)->get();
        return response()->json(compact('bills'));
    }

    public function updateBillStatus($id)
    {
        //return "hello";
        $bill = Bills::find($id);
        if($bill->status == 1){
            $bill->status = 2;
        }else{
            $bill->status = 1;
        }
        $bill->save();
        return response()->json(['bill status updated successfully']);
    }
}
