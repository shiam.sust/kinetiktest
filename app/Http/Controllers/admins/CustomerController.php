<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;


class CustomerController extends Controller
{
    public function store(Request $request){
        $customer = new Customer();

        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->password = Hash::make($request->password);
        $customer->address = $request->address;

        $customer->save();

        return response()->json(['new customer added!']);
    }

    public function getCustomers()
    {
        $customers = Customer::all();
        return response()->json(compact('customers'));
    }

    public function getTheCustomer($id)
    {
        $customer = Customer::find($id);
        return response()->json(compact('customer'));
    }

    public function editCustomer(Request $request)
    {
        $customer = Customer::find($request->customerId);
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->address = $request->address;

        $customer->save();

        return response()->json(['customer updated successfully!']);

    }

    public function deleteCustomer($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return response()->json(['customer deleted successfully!']);
    }
}
