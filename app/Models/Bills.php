<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bills extends Model
{
    protected $table = 'bills';
    protected $primaryKey = 'id';

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
}
