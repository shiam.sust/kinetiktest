import React, {useState, useEffect} from 'react';
import { useHistory, useParams } from "react-router-dom";
import {editCustomer} from './Requests';
import axios from 'axios';

const url = 'http://kinetiktest.me/api/admins/get-customer-info';


function EditCustomer() {
    const { customerId } = useParams();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    let history = useHistory();

    useEffect(() => {
        async function fetchData() {
            const request = await axios.get(`${url}/${customerId}`);
            console.log(request.data.customer);
            const tempData = request.data.customer;
            setName(tempData.name);
            setEmail(tempData.email);
            setAddress(tempData.address);
        }
        fetchData();
    }, []);


    const handleChangeName = (e) => {
        setName(e.target.value);
    }

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    }

    const handleChangeAddress = (e) => {
        setAddress(e.target.value);
    }

    const submitEditCustomerForm = (e) => {
        e.preventDefault();

        const customerData = {
            name: name,
            email: email,
            address: address,
            customerId: customerId
        }

        //console.log(newCustomerData);

        editCustomer(customerData).then(res => {
            if(res){
                //console.log("successfully updated customer");
                history.push('/customers');
            }
        })
    }

    return (
        <div className="add-new-customer">
            <div className="card mt-5" style={{width: '28rem' }}>
                <div className="card-header">
                    Add new Customer
                </div>
                <div className="card-body">
                    <form onSubmit={submitEditCustomerForm}>
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input
                                type="text" 
                                className="form-control"
                                id="name"
                                aria-describedby="nameHelp" 
                                name="name"
                                value={name}
                                onChange={handleChangeName}
                                placeholder="Enter name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input
                                type="email" 
                                className="form-control" 
                                id="exampleInputEmail1" 
                                aria-describedby="emailHelp" 
                                name="email"
                                value={email}
                                onChange={handleChangeEmail}
                                placeholder="Enter email"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="address">Address</label>
                            <input
                                type="text" 
                                className="form-control"
                                id="address"
                                aria-describedby="addressHelp" 
                                name="address"
                                value={address}
                                onChange={handleChangeAddress}
                                placeholder="Enter address"/>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            
            
            </div>
        
        
    </div>
    )
}

export default EditCustomer;
