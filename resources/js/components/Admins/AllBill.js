import React, {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import {showBills, changeBillStatus} from "./Requests";

function AllBill() {
    const { customerId } = useParams();
    const [bills, setBills] = useState([]);
    const [status, setStatus] = useState(true);
    const [showStatus, setShowStatus] = useState(1);

    useEffect(() => {
        showBills(customerId).then(res => {
            if(res){
                //console.log(res.bills);
                setBills(res.bills);
            }else{
                setError("error");
            }
        })

    }, [status]);

    function toggleBillStatus(id){
        console.log(id);
        //const request = await axios.post(`${url2}/${id}`);
        changeBillStatus(id).then(res => {
            if(res){
                //console.log(res.bills);
                setStatus(!status); 
            }else{
                setError("error");
            }
        })
    }

    const showAll = () => setShowStatus(1);
    const showDue = () => setShowStatus(2);
    const showPaid = () => setShowStatus(3);

    return (
        <div className="customer">
            <h3>Bills</h3>
            <div className="mb-5">
                <button onClick={showAll}>All</button>
                <button onClick={showDue}>Only Due</button>
                <button onClick={showPaid}>Only Paid</button>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Month</th>
                        <th scope="col">year</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {showStatus === 1 && (bills.map((bill) => {
                        const bill_id = bill.id;
                        return(
                            <tr>
                                <td>{bill.month}</td>
                                <td>{bill.year}</td>
                                <td>{bill.amount}</td>
                                <td>{bill.status === 1 ? "due" : "paid"}</td>
                                <td><button onClick={() => toggleBillStatus(bill_id)}>Change Status</button></td>
                            </tr>
                        );
                        }
                    ))}
                    {showStatus === 2 && (bills.map((bill) => {
                        const bill_id = bill.id;
                        if(bill.status === 1){
                            return(
                                <tr>
                                    <td>{bill.month}</td>
                                    <td>{bill.year}</td>
                                    <td>{bill.amount}</td>
                                    <td>{bill.status === 1 ? "due" : "paid"}</td>
                                    <td><button onClick={() => toggleBillStatus(bill_id)}>Change Status</button></td>
                                </tr>
                            );
                        }
                        }
                    ))}
                    {showStatus === 3 && (bills.map((bill) => {
                        const bill_id = bill.id;
                        if(bill.status === 2){
                            return(
                                <tr>
                                    <td>{bill.month}</td>
                                    <td>{bill.year}</td>
                                    <td>{bill.amount}</td>
                                    <td>{bill.status === 1 ? "due" : "paid"}</td>
                                    <td><button onClick={() => toggleBillStatus(bill_id)}>Change Status</button></td>
                                </tr>
                            );
                        }
                        }
                    ))}
                </tbody>
            </table>    
        </div>
    )
}

export default AllBill;
