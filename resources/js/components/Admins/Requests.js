import axios from 'axios';

const url = 'http://kinetiktest.me/api/';
const token = { Authorization: `bearer ${localStorage.adminsToken}` };

const addCustomer = (newUserData) => {
    return axios.post(
        url+'admins/add-customer',
        {
            name: newUserData.name,
            email: newUserData.email,
            password: newUserData.password,
            address: newUserData.address
        },
        {
            headers: {
                'content-type': 'application/json'
            }
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const showCustomer = () => {
    return axios
        .get( url+'admins/customers',
        {
            hearders: token
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const editCustomer = (customerData) => {
    return axios.post(
        `${url}admins/edit-customer`,
        {
            customerId: customerData.customerId,
            name: customerData.name,
            email: customerData.email,
            address: customerData.address
        },
        {
            headers: {
                'content-type': 'application/json'
            }
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const deleteCustomer = (customerId) => {
    return axios
        .post( `${url}admins/delete-customer/${customerId}`,
        {
            hearders: token
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const addBill = (newUserData) => {
    return axios.post(
        url+'admins/add-bill',
        {
            customerId: newUserData.customerId,
            month: newUserData.month,
            year: newUserData.year,
            amount: newUserData.amount
        },
        {
            headers: {
                'content-type': 'application/json'
            }
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const showBills = (customerId) => {
    return axios
        .get( `${url}admins/bills/${customerId}`,
        {
            hearders: token
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

const changeBillStatus = (billId) => {
    return axios
        .post( `${url}admins/change-bill-status/${billId}`,
        {
            hearders: token
        }
    )
    .then(res => {
        return res.data;
    })
    .catch(err => {
        console.log(err);
    });
}

export {
    addCustomer,
    showCustomer,
    editCustomer,
    deleteCustomer,
    addBill,
    showBills,
    changeBillStatus
};
    