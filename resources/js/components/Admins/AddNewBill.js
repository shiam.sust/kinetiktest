import React, {useState}  from 'react';
import { useParams } from "react-router-dom";
import {addBill} from './Requests';
import { useHistory } from "react-router-dom";



function AddNewBill() {
    const { customerId } = useParams();
    const [month, setMonth] = useState("");
    const [year, setYear] = useState("");
    const [amount, setAmount] = useState("");
    let history = useHistory();

    const handleChangeMonth = (e) => {
        setMonth(e.target.value);
    }

    const handleChangeYear = (e) => {
        setYear(e.target.value);
    }

    const handleChangeAmount = (e) => {
        setAmount(e.target.value);
    }

    const submitNewBill = (e) => {
        e.preventDefault();

        const newBillData = {
            customerId: parseInt(customerId),
            month: month,
            year: year,
            amount: parseInt(amount)
        }

        console.log(newBillData);

        addBill(newBillData).then(res => {
            if(res){
                console.log("successfully bill added");
                history.push('/customers');
            }
        })
    }

    return (
        <div className="add-new-bill">
            <div className="card mt-5" style={{width: '20rem' }}>
                <div className="card-header">
                    Add new Customer
                </div>
                <div className="card-body">
                    <form onSubmit={submitNewBill}>
                        <div className="form-group">
                            <label htmlFor="name">Month</label>
                            <input
                                type="text" 
                                className="form-control"
                                id="month"
                                aria-describedby="monthHelp" 
                                name="month"
                                value={month}
                                onChange={handleChangeMonth}
                                placeholder="Enter month"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="year">Year</label>
                            <input
                                type="text" 
                                className="form-control" 
                                id="year" 
                                aria-describedby="yearHelp" 
                                name="year"
                                value={year}
                                onChange={handleChangeYear}
                                placeholder="Enter Year"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="amount">Amount</label>
                            <input 
                                type="amount" 
                                className="form-control" 
                                id="amount" 
                                name="amount"
                                value={amount}
                                onChange={handleChangeAmount}
                                placeholder="Amount"/>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            
            
            </div>
        </div>
    )
}

export default AddNewBill;
