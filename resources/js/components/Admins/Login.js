import React, { useState } from "react";
import login from "./loginRequest";
import { useHistory } from "react-router-dom";

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    //const [homeRoute, setHomeRoute] = useState("/home");
    let history = useHistory();

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    }

    const submitLoginForm = (e) => {
        e.preventDefault();

        const adminsData = {
            email: email,
            password: password
        }

        //console.log(adminsData);
        login(adminsData).then(res => {
            if(res){
                console.log("success login");
                history.push('/home');
            }else{
                setError("email or password is wrong");
            }
        })
    }

    const errorMessage = <div className="alert alert-danger">{error}</div>;

    return(
    <div>
        <div className="card mt-5" style={{width: '18rem' }}>
            {error ? errorMessage : null}
            <div className="card-header">
                Login
            </div>
            <div className="card-body">
                <form onSubmit={submitLoginForm}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input
                            type="email" 
                            className="form-control" 
                            id="exampleInputEmail1" 
                            aria-describedby="emailHelp" 
                            name="email"
                            value={email}
                            onChange={handleChangeEmail}
                            placeholder="Enter email"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input 
                            type="password" 
                            className="form-control" 
                            id="exampleInputPassword1" 
                            value={password}
                            onChange={handleChangePassword}
                            placeholder="Password"/>
                    </div>
                    
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        
        
        </div>
        
        
    </div>);
}

export default Login;