import React, {useState, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import {showCustomer, deleteCustomer} from "./Requests";


//const url = 'http://kinetiktest.me/api/admins/customers';
//const url2 = 'http://kinetiktest.me/api/admins/delete-customer';

function Customer() {
    const [customers, setCustomers] = useState([]);
    const [status, setStatus] = useState(true);

    let history = useHistory();

    useEffect(() => {
        //const request = await axios.get(url);
        showCustomer().then(res => {
            if(res){
                //console.log(res.customers);
                setCustomers(res.customers);
                //history.push('/home');
            }else{
                setError("error");
            }
        })
        
        //fetchData();
    }, [status]);

    const createNewBill = (id) => {
        //console.log(id);      
        history.push(`/add-new-bill/${id}`);
    }

    const showBills = (id) => {
        //console.log(id);      
        history.push(`/bills/${id}`);
    }

    const editCustomer = (id) => {
        console.log(id);      
        history.push(`/edit-customer/${id}`);
    }

    async function delCust(id){
        console.log(id);
        //const request = await axios.post(`${url2}/${id}`);
        deleteCustomer(id).then(res => {
            if(res){
                //console.log(res.bills);
                setStatus(!status); 
            }else{
                setError("error");
            }
        })
        //console.log(request);    
        //setStatus(!status); 
    }

    return (
        <div className="customer">
            <h3>Customers</h3>
            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Address</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((customer) => {
                        const customer_id = customer.id;
                        return(
                            <tr>
                                <td>{customer.name}</td>
                                <td>{customer.email}</td>
                                <td>{customer.address}</td>
                                <td><button onClick={() => editCustomer(customer_id)}>edit</button></td>
                                <td><button onClick={() => delCust(customer_id)}>delete</button></td>
                                <td><button onClick={() => createNewBill(customer_id)}>create new bill</button></td>
                                <td><button onClick={() => showBills(customer_id)}>show bills</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>    
        </div>
    );
}

export default Customer;
