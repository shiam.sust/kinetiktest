import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import {addCustomer} from './Requests';

function AddNewCustomer() {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [address, setAddress] = useState("");
    let history = useHistory();


    const handleChangeName = (e) => {
        setName(e.target.value);
    }

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    }

    const handleChangeAddress = (e) => {
        setAddress(e.target.value);
    }

    const submitAddCustomerForm = (e) => {
        e.preventDefault();

        const newCustomerData = {
            name: name,
            email: email,
            password: password,
            address: address
        }

        console.log(newCustomerData);

        addCustomer(newCustomerData).then(res => {
            if(res){
                console.log("successfully add customer");
                history.push('/customers');
            }
        })
    }

    return (
        <div className="add-new-customer">
            <div className="card mt-5" style={{width: '28rem' }}>
                <div className="card-header">
                    Add new Customer
                </div>
                <div className="card-body">
                    <form onSubmit={submitAddCustomerForm}>
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input
                                type="text" 
                                className="form-control"
                                id="name"
                                aria-describedby="nameHelp" 
                                name="name"
                                value={name}
                                onChange={handleChangeName}
                                placeholder="Enter name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input
                                type="email" 
                                className="form-control" 
                                id="exampleInputEmail1" 
                                aria-describedby="emailHelp" 
                                name="email"
                                value={email}
                                onChange={handleChangeEmail}
                                placeholder="Enter email"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input 
                                type="password" 
                                className="form-control" 
                                id="exampleInputPassword1" 
                                name="password"
                                value={password}
                                onChange={handleChangePassword}
                                placeholder="Password"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="address">Address</label>
                            <input
                                type="text" 
                                className="form-control"
                                id="address"
                                aria-describedby="addressHelp" 
                                name="address"
                                value={address}
                                onChange={handleChangeAddress}
                                placeholder="Enter address"/>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            
            
            </div>
        
        
    </div>
    )
}

export default AddNewCustomer
