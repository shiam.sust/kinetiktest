import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Sidebar from './Sidebar';
import Customer from './Customer';
import AddNewBill from './AddNewBill';
import AllBill from './AllBill';
import EditCustomer from './EditCustomer';
import AddNewCustomer from './AddNewCustomer';
import '../../../css/app.css';

function Home() {
    return (
        <div className="home">
            <Router>
                <Sidebar />
                    <Switch>
                        <Route path="/customers" component={Customer} />
                        <Route path="/add-new-customers" component={AddNewCustomer} />
                        <Route path="/add-new-bill/:customerId" component={AddNewBill} />
                        <Route path="/bills/:customerId" component={AllBill} />
                        <Route path="/edit-customer/:customerId" component={EditCustomer} />
                    </Switch>
            </Router>
        </div>
    )
}

export default Home

//rfce