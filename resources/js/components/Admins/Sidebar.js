import React from 'react';
import {Link} from "react-router-dom";

function Sidebar() {
    return (
        <div className="sidebar">
            <div className="links">
                <Link to="/home">Home</Link>
                <Link to="/customers">Customers </Link>
                <Link to="/add-new-customers">add-new-customers</Link>
            </div>
        </div>
    )
}

export default Sidebar;
