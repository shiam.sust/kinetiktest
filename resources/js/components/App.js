import React from 'react';
import ReactDOM from 'react-dom';
import Login from './Admins/Login';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./Admins/Home";


function App() {
    return (
        <div className="container">
            <Router>
                <Route path="/home" component={Home} />
                <Route exact path="/adminsLogin" component={Login} />
          </Router>
        </div>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
