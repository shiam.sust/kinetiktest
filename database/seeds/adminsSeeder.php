<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class adminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
                'name' => 'shiam',
                'email' => 'shiam@gmail.com',
                'password' =>Hash::make('123456')
            ],
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' =>Hash::make('123456')
            ]
        ]);
    }
}
